# SST cloud application

It is the game super star trek(our main application), which we integrated with service box. During implementation this game, we had a few architecture, but it was only stages, to reach microservices architecture.

# Service Box
Service Box is internal application created in Oak, as tool for easy generate microservices application skeleton.   

You can think of service box as framework, which gives us microservice architecture, but now it's a part of our application. 
## Microservices
Common used architecture, gives us highly maintainable and testable services, which are loosely coupled and
 independently deployable.

Service Box generate 4 main services:
* Gateway service
* Discovery service
* Config service
* IDM

Our application contain service SST, which is not part of Service Box itself. 
SST service is, a part of applications with our game. It includes few modules:
 * Client(simple terminal client interface, which will be finally replaced by GUI from frontend)
 * Core(main module of our backend app)
 * Bus(module with our rabbitBus(queue/bus))
 * Data(module without business logic, it contain only data)
 * I18N(module responsible for display information in terminal client(with different languages, based on localization))
SST also contain two simple services:
* CoreService(core of our application)
* ClientService(temporary service with terminal interface, useful in developing backend app)

## Gateway Service
It is front side of our application. We provide single entry point, and then gateway route request to
 appropriate service. Gateway service also contain security, so we don't need secure every microservice separately.  

## Discovery Service
Discovery service is responsible determining the network locations of available service instances. It is especially
 important, when services are created dynamically, because without discovery, application wouldn't know where find
  some services.  

## ConfigService
Config service is part of application, which get configuration from sst-cloud-config repository. You can see
 configuration in sst-cloud-config module. It's important, that you need to push changes, before you run application
  again. Otherwise application will work same, as earlier, because, as I said, it took configuration form repository!
  
## IDM
IDM(Identity management) is determines whether a user has access to application, but also sets the level of access and
 permissions a user has on a particular part of application.
 
## ROOTS
Framework created in oak, used in IDM. This library is as simple as possible set of tools for building DDD oriented applications using CQRS and Event Sourcing approaches.


       
             